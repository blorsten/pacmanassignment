package com.peterwitt.pacman.Game;

public class Vector2 {
    public static Vector2 zero = new Vector2(0,0);

    public float x;
    public float y;

    public Vector2(float x, float y){
        this.x = x;
        this.y = y;
    }

    public Vector2 add(Vector2 other){
        x += other.x;
        y += other.y;

        return this;
    }

    public Vector2 multiply(int value){
        x *= value;
        y *= value;

        return this;
    }

    public Vector2 multiply(float value){
        x *= value;
        y *= value;

        return this;
    }

    public Vector2 subtract(Vector2 other){
        return new Vector2(x-other.x, y-other.y);
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
    }
}
