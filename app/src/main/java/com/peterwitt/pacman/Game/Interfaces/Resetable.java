package com.peterwitt.pacman.Game.Interfaces;

public interface Resetable {
    void reset();
}
