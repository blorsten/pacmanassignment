package com.peterwitt.pacman.Game.Interfaces;

public interface Updatable {
    void update(float deltatime);
}
