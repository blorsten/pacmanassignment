package com.peterwitt.pacman.Game.Interfaces;

public interface Destroyable {
    void OnDestroy();
}
