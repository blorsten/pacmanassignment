package com.peterwitt.pacman.Game;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.util.Size;
import android.view.View;

import com.peterwitt.pacman.Game.Interfaces.Collidable;
import com.peterwitt.pacman.Game.Interfaces.Drawable;
import com.peterwitt.pacman.Game.Interfaces.Resetable;
import com.peterwitt.pacman.Game.Interfaces.Updatable;
import com.peterwitt.pacman.Listeners.SwipeDirection;
import com.peterwitt.pacman.Listeners.SwipeListener;
import com.peterwitt.pacman.R;

public class Player extends GameObject implements SwipeListener, Updatable, Drawable, Resetable, Collidable{

    public float speed = 500;
    public int size = 100;
    public Bitmap sprite;
    public int lives = 3;
    public int score = 0;
    private SwipeDirection moveDirection = SwipeDirection.Right;
    private boolean hasBeenHit;

    @Override
    public void onSwipe(SwipeDirection direction) {
        moveDirection = direction;
    }

    public Player(String tag){
        this.tag = tag;
    }

    @Override
    public void draw(View view, Canvas canvas, Paint paint) {

        int id = 0;

        switch (moveDirection){

            case Right:
                id = R.drawable.pacman_right;
                break;
            case Left:
                id = R.drawable.pacman_left;
                break;
            case Up:
                id = R.drawable.pacman_up;
                break;
            case Down:
                id = R.drawable.pacman_down;
                break;
        }

        sprite = BitmapFactory.decodeResource(view.getResources() ,id);
        canvas.drawBitmap(Bitmap.createScaledBitmap(sprite, size, size, false), position.x, position.y, paint);
    }

    @Override
    public void reset() {
        lives = 3;
        score = 0;
        respawn();
    }

    @Override
    public void update(float deltatime) {
        hasBeenHit = false;

        Vector2 translation = Vector2.zero;

        switch (moveDirection)
        {
            case Right:
                translation = new Vector2(1,0);
                break;
            case Left:
                translation = new Vector2(-1,0);
                break;
            case Up:
                translation = new Vector2(0,-1);
                break;
            case Down:
                translation = new Vector2(0,1);
                break;
        }

        Translate(translation.multiply(speed * deltatime));

        if(position.x < 0)
            position.x = 0;
        if((position.x + size) > GameWorld.instance.width)
            position.x = GameWorld.instance.width - size;
        if(position.y < 0)
            position.y = 0;
        if((position.y + size) > GameWorld.instance.height)
            position.y = GameWorld.instance.height - size;

    }

    @Override
    public int getCollider() {
        return size/2;
    }

    public void respawn(){

        do{
        position.x = GameWorld.instance.random.nextFloat() * GameWorld.instance.width;
        position.y = GameWorld.instance.random.nextFloat() * GameWorld.instance.height;
        }while(distanceToNearestEnemy() < 500);
    }

    private float distanceToNearestEnemy(){
        double lowestDist = 999999f;

        for(GameObject go : GameWorld.instance.GameObjects){
            if(go.tag == "Enemy"){
                Vector2 dir2enemy = go.position.subtract(position);
                if(dir2enemy.magnitude() < lowestDist)
                    lowestDist = dir2enemy.magnitude();
            }
        }

        return (float)lowestDist;
    }

    @Override
    public void onCollision(GameObject other) {
        if(other.tag == "Enemy" && !hasBeenHit){
            hasBeenHit = true;
            GameWorld.instance.deleteGameObject(other);
            lives --;
            respawn();

            if(lives < 1){
                GameWorld.instance.Reset();
            }
        }

        else if(other.tag == "Pill"){
            GameWorld.instance.deleteGameObject(other);
            score++;
        }
    }
}
