package com.peterwitt.pacman.Listeners;

public interface SwipeListener {
    void onSwipe(SwipeDirection direction);
}
