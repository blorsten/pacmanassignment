package com.peterwitt.pacman.Listeners;

public enum SwipeDirection {
    Right, Left, Up, Down
}
